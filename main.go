package main

import app2 "fyner/app"

func main() {
	app := app2.New("dev.cdel.fyner.test")
	win := app.NewWindow("Fyner Test")
	win.Show()
	app.Run()
}
