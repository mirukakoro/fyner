package app

import (
	"fmt"
	"fyne.io/fyne"
	"log"
	"unicode"
)

type Action interface {
	Callback(*Window, string) error
	Icon() fyne.Resource
	Label() string
}

type NativeAction struct {
	callback func(*Window, string) error
	icon     fyne.Resource
	label    string
}

func (n NativeAction) Callback(w *Window, child string) error {
	return n.callback(w, child)
}

func (n NativeAction) Icon() fyne.Resource {
	return n.icon
}

func (n NativeAction) Label() string {
	return n.label
}

type Window struct {
	fyne.Window
	app *App

	*Actions
}

func (w *Window) SyncActions() {
	w.SyncToMenu(w)
}

type Actions struct {
	actions map[string]Action
}

func (a *Actions) SyncToMenu(w *Window) {
	menuitems := make([]*fyne.MenuItem, 0)
	menuitems = append(menuitems,
		fyne.NewMenuItem("Search", func() {
			fmt.Println("wip search")
		}),
	)
	for _, action := range a.actions {
		lbl := action.Label()
		callback := action.Callback
		fmt.Println(action.Label())
		menuitems = append(menuitems,
			fyne.NewMenuItem(lbl, func() {
				log.Printf(`calling callback for action "%s"`, lbl)
				err := callback(w, "")
				if err != nil {
					fyne.LogError(fmt.Sprintf(`failed callback for action "%s"`, lbl), err)
				}
			}),
		)
	}
	settingsAction := a.Action("settings")
	w.SetMainMenu(fyne.NewMainMenu(
		fyne.NewMenu(
			"Actions",
			menuitems...,
		),
		fyne.NewMenu(
			"Settings",
			fyne.NewMenuItem(
				settingsAction.Label(),
				func() {
					err := settingsAction.Callback(w, "")
					if err != nil {
						fyne.LogError(fmt.Sprintf(`Callback for settingsAction "%s"`, settingsAction.Label()), err)
					}
				},
			),
		),
	))
}

func (a *Actions) Action(key string) Action {
	for _, c := range key {
		if !unicode.IsLetter(c) {
			panic("action key must not contain non letter characters")
		}
	}
	// maybe have a Go like action key:
	// Edit.Copy
	// Edit.Cut
	// Edit.copy (can only be accessed from Copy, Cut, just like Go)
	return a.actions[key]
}

func (a *Actions) SetAction(key string, action Action) {
	for _, c := range key {
		if !unicode.IsLetter(c) {
			panic("action key must not contain non letter characters")
		}
	}
	a.actions[key] = action
}
